var end_point="./files.php";
var selected="/";

//------------------------------------------------------
function load_component(id,template){
//------------------------------------------------------
    var url=end_point+"?select="+encodeURIComponent(selected)+"&template="+encodeURIComponent(template);
    $(id).load(url);

}

//------------------------------------------------------
function select(path,plugin="list"){
//------------------------------------------------------
    selected=path;

    load_component("#AppCurrentPath","./templates/node/path.php");
    load_component("#AppTree","./templates/node/tree.php");
    load_component("#AppContent","./templates/node/"+plugin+".php");

}
//------------------------------------------------------

load_component("#AppHeader","./templates/app/app-bar.php");
select("/");
//load_component("#AppContent","./templates/node/stream.php");

