<?php
include("./../libraries/import.php");


//=================================================================
class LocalFile extends DiscNode {
//=================================================================
    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();
    }
    //-------------------------------------------------------------
}
//=================================================================
class LocalDir extends DiscNode {
//=================================================================
    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();
    }
    //-------------------------------------------------------------

}

//=================================================================

$main_path=$_SERVER["DOCUMENT_ROOT"];

//$main_path="/home/www/html/seed-env/src";
//$main_path="/home/www/html/model";
$select=rtrim($_GET["select"], '/');
$template=$_GET["template"];

//$root=new LocalDir(null,["path"=>"./../local/data/store"]);
$root=new LocalDir(null,["path"=>$main_path]);
new InitDirs($root,["event"=>"setup","cls"=>"LocalDir"]);
$root->setup();
if($select=="/"){
    $node=$root;
}else if(is_dir($main_path.$select)){
    $node=$root->make_node($select,"LocalDir",["path"=>$main_path.$select]);
    new InitDirs($node,["event"=>"setup","cls"=>"LocalDir"]);
    new InitFiles($node,["event"=>"setup","cls"=>"LocalFile"]);
    $node->setup();
}else{
    $node=$root->make_node($select,"LocalFile",["path"=>$main_path.$select]);
    $node->setup();
}


//tree($root);
include($template);

?>
