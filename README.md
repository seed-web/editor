
# interface de l'éditeur

## objectif et fonctionalités

une interface de naviguation et d'édition pour l'environnment Seed.


## services php

### index.php

crée la page html

charge la page html controlable par l'utilisateur.

```
end-point : ./index.php
```


### template.php

gère les templates et associe les données

```
end-point : ./files.php
```


description de la requete :

```
method : GET

- select : chemin relatif d'un fichier ou répertoire sur le serveur
- template : le chemin relatif d'un template dans le répertoire ./templates
```

### data.php

ajouter des données et les visualiser

```
end-point : ./data.php
```

envoyer des données :

```
method : POST

- input : json format
   - select path to data
```


actions sur les données : 

```
method : GET

- action :
   - read
      - select : node path
   - create
      - select root dir
      - name dir name
   - delete
      - select : node path
```

## modele scss

Le css est écrit en scss pour faciliter l'écriture.

- variables
- fonctions

les fichiers nécéssaires sont dans le répertoire ./sass

pour génerer le css dans le répertoire ./css, 
taper la commande dans le terminal :
```
bash sass.sh
```

