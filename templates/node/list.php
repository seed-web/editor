        <?php
        include("./templates/node/media.php");

        ?>
<div id="NodeContent" class="node-content">

        <?php
            include("./templates/node/title.php");
        ?>


    <div id="NodeData" class="node-data">
    <div class="Xcontent">
        <?php
$background=$node->find("background.jpg");
if($background){
show_image($background,"height:200px;overflow:hidden;");
}
$background=$node->find("background.png");
if($background){
show_image($background,"height:200px;overflow:hidden;");
}

?>
</div>

<div class="content markdown">
<?php
$readme=$node->find("README.md");
if($readme){
plot_content($readme);
}
?>
</div>


<div class="content">
<?php

$elt=$node->find("index.php");
if($elt){
show_embeded($elt);
}
$elt=$node->find("index.html");
if($elt){
show_embeded($elt);
}

        ?>
    </div>




    <div class="content">
        <?php

show_media($node);


        ?>
    </div>



    </div>
</div>


<div id="NodeTree" class="node-bar">

    <div class="content">
        <?php
$background=$node->find("index.png");
if($background){
show_image($background,"height:400px;overflow:hidden;padding-left:120px;padding-right:120px;");
}


        ?>

<div class="content">
<?php
foreach($node->search("*/[LocalDir]")->iter() as $child){
?>
<div class="children"  onclick="select('<?=$child->path()?>','list')">
 <h2> <i class="bi bi-folder"></i><?=$child->get("name")?></h2>
</div>
<?php
}
?>
</div>

<?php
foreach($node->search("*/[LocalFile]")->iter() as $child){
?>
<div class="children"  onclick="select('<?=$child->path()?>','list')">
 <h2> <i class="bi file-earmark"></i><?=$child->get("name")?></h2>
</div>
<?php
}
?>
    </div>


</div>


