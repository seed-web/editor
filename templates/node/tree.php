<?php
function make_node($elt,$limit,$level=0){

if($level ==$limit){
    return;
}
?>
<li>
<span onclick="select('<?=$elt->path()?>','list')">
<i class="bi bi-box-fill"></i>
<span><?=$elt->get("name")?></span>
</span>

<ul>
<?php
foreach($elt->search("*/[LocalDir]")->iter() as $child){
    make_node($child,$limit,$level+1);
}
?>
</ul>
</li>
<?php
}
?>
<div class="tree">
<ul>
<?php
make_node($root,3);
?>
</ul>
</div>
