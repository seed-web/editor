<?php

//=========================================================
function show_media($node){
//=========================================================

        if($node->is_dir()){
            //list_dirs($node);
            //list_files($node);
        }else if( $node->is_content()){
            plot_content($node);
        }else if($node->is_image()){
            show_image($node);
        }else if($node->is_embeded()){
            show_embeded($node);
        }else if($node->is_video()){
            show_video($node);
        }else if($node->is_audio()){
            show_audio($node);
        }
}
//=========================================================
function list_dirs($node){
//=========================================================

?>

<div class="content">
<?php
foreach($node->search("*/[LocalDir]")->iter() as $child){
?>
<div class="children"  onclick="select('<?=$child->path()?>')">
 <h2> <i class="bi bi-folder"></i><?=$child->get("name")?></h2>
</div>
<?php
}
?>
</div>

<?php
}
//=========================================================
function list_files($node){
//=========================================================

?>


<div id="NodeDataContent" class="content">
<?php
foreach($node->search("*/[LocalFile]")->iter() as $child){
?>
<div class="children"  onclick="select('<?=$child->path()?>')">
 <h2> <i class="bi bi-file-earmark"></i> <?=$child->get("name")?></h2>
</div>
<?php
}
?>
</div>

<?php
}
//=========================================================
function show_audio($node){
//=========================================================

?>

<div class="content">
<audio src="<?=url($node)?>" style="width:100%;" controls></iframe>
</div>
<?php
}
//=========================================================
function show_embeded($node){
//=========================================================

?>

<div class="content">
<iframe src="<?=url($node)?>" style="width:100%;height:80vh;" controls></iframe>
</div>
<?php
}
//=========================================================
function show_video($node){
//=========================================================

?>
<div class="content">
<video src="<?=url($node)?>" style="width:100%;height:100%;" controls></video>
</div>
<?php
}
//=========================================================
function show_image($node,$style=""){
//=========================================================

?>

<div class="content" style="<?=$style?>">
<img src="<?=url($node)?>" style="width:100%"></img>
</div>
<?php
}
//=============================================================
function plot_datanodes($node){
//=============================================================
?>
<li>
<span>
<i class="bi bi-box-fill"></i>
</span>

<?php
plot_data($node);
?>
<ul>
<?php
foreach($node->children->iter() as $child){
    plot_datanodes($child);
}

?>
</ul>
</li>
<?php
}
//=============================================================
function plot_data($node){
//=============================================================
?>
 <table class="xml-table">
<?php
foreach( $node->keys() as $key){
?>
  <tr>
    <td class="attr"><?=$key?></td>
    <td class="data"><?=$node->get($key)?></td>
  </tr>
<?php
}
?>
</table>
<?php
}
//=============================================================
function plot_content($node){
//=============================================================
if($node->get("ext")=="xml"){

?>
<div class="content xml-content">
<?php

    $xml_parser=new DomXmlRead();
    $new_node=$xml_parser->readfile($node->get("path"));
    plot_datanodes($new_node);

?>
</div>

<?php
}else if($node->get("ext")=="md"){

        $url=$node->get("path");
        //echo $url;

        $output = shell_exec("pandoc -f markdown -t html $url");
        echo $output;
}

}
//=============================================================

?>





