    <div id="NodeHeader"  class="node-title">
        <h1>
            <i class="bi bi-box-fill"></i> 
            <?=$node->get("name")?>
        </h1>

        <span  onclick="select('<?=$node->path()?>','edit')"><i class="bi bi-pencil-square"></i></span>
        <span onclick="select('<?=$node->path()?>','stream')"><i class="bi bi-eye"></i> </span>
        <span onclick="select('<?=$node->path()?>','list')"><i class="bi bi-info-circle"></i> </span>
    </div>
